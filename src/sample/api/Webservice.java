package sample.api;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Webservice {

    public static String requestUrl(String method, String endpoint) throws Exception {
        String res = null;
        URL url = new URL("http://localhost:8001/api/v1/" + endpoint);
        String readLine;
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        connection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample here

        int responseCode = connection.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) {

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuilder response = new StringBuilder();

            while ((readLine = in.readLine()) != null) {
                response.append(readLine);
            }
            in.close();
            res = response.toString();
//            convertObject(res);

        } else {
            System.out.println("Request Url Not Work");
        }
        System.out.println("RESSSS:" + res);
        return res;
    }

    private static void convertObject(String str) {
        JSONObject jsonR = null;
        try {
            jsonR = (JSONObject) new JSONParser().parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("response=" + jsonR.get("response"));
        System.out.println("results=" + jsonR.get("results"));
        String rr = jsonR.get("response").toString();
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject) new JSONParser().parse(rr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("responseA=" + jsonObject.get("code"));
    }
}

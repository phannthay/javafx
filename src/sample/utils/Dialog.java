package sample.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Region;

public class Dialog {
    public static void showDialog(String message){
        Alert alert = new Alert(Alert.AlertType.WARNING,message, ButtonType.OK);
        alert.getDialogPane().minHeight(Region.USE_PREF_SIZE);
        alert.show();
    }
}

package sample.utils;

import javafx.scene.Parent;
import javafx.scene.Scene;

public class Constant {
    public static Parent root = null;
    public static Scene scene = null;
    public static String baseUrl = "http://localhost:8000/api/v1/";

    public static String Post = "POST";
    public static String Get = "GET";
    public static String Put = "PUT";
    public static String Delete = "DELETE";

    public static String ENCRYPTION_KEY = "aesEncryptionKey";
    public static String VECTOR = "encryptionIntVec";

}

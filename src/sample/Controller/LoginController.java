package sample.Controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import sample.api.Webservice;
import sample.utils.AESUtil;
import sample.utils.Constant;
import sample.utils.Dialog;
import sample.utils.WorkIndicatorDialog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable, EventHandler<ActionEvent> {

    @FXML
    private GridPane gridPane = new GridPane();
    @FXML
    private Label email, password = new Label();
    @FXML
    private Button btnSubmit, btnClear = new Button();
    @FXML
    private TextField tfEmail, tfPassword = new TextField();

    private WorkIndicatorDialog wd = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.initUI();
    }

    private void initUI() {

        gridPane.getStyleClass().add("main-login");
        email.getStyleClass().add("text-label");
        password.getStyleClass().add("text-label");

        btnSubmit.setOnAction(this);
        btnClear.setOnAction(this);
    }

    private void clearForm() {
        tfEmail.setText("");
        tfPassword.setText("");
    }

    private void validateForm(String value) {
        if (value.isEmpty()) {
            System.out.println("Field can not empty!");
        }
    }

    private void login() {
        if (tfEmail.getText().isEmpty() || tfEmail.getText().isEmpty()) {
            Dialog.showDialog("Please fill username and password");
            return;
        }
        String password = AESUtil.encrypt(Constant.ENCRYPTION_KEY, Constant.VECTOR, tfPassword.getText());
        System.out.println("PASSWORD:" + password);
        try {
            String response = Webservice.requestUrl(Constant.Post, "laundry/auth/mobile/app-lc-login?" + "username=" + tfEmail.getText() + "&password=" + password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void getAllUser() {
        try {
            String response = Webservice.requestUrl(Constant.Get, "user/all");
            System.out.println("getAllUser " + response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void MyGetRequest() throws Exception {
//        URL urlForGetRequest = new URL("https://jsonplaceholder.typicode.com/posts/1");
        URL urlForGetRequest = new URL("http://localhost:8000/api/v1/user/all");
        String readLine = null;
        HttpURLConnection conection = (HttpURLConnection) urlForGetRequest.openConnection();
        conection.setRequestMethod("GET");
        conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample here
        int responseCode = conection.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) {

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conection.getInputStream()));
            StringBuffer response = new StringBuffer();

            while ((readLine = in.readLine()) != null) {
                response.append(readLine);
            }
            in.close();
            // print result
            String r = response.toString();
            JSONObject jsonR = (JSONObject) new JSONParser().parse(r);
            System.out.println("response=" + jsonR.get("response"));
            System.out.println("results=" + jsonR.get("results"));
            String rr = jsonR.get("response").toString();
            JSONObject jsonObject = (JSONObject) new JSONParser().parse(rr);
            System.out.println("responseA=" + jsonObject.get("code"));

            //GetAndPost.POSTRequest(response.toString());
        } else {
            System.out.println("GET NOT WORKED");
        }
    }

    @Override
    public void handle(ActionEvent event) {

        if (event.getSource() == btnSubmit) {
            try {
//                MyGetRequest();
//                getAllUser();
                onShowLoading(event);
                login();
            } catch (Exception e) {
                e.printStackTrace();
            }
            validateForm(tfEmail.getText());
            System.out.println("This is my email " + tfEmail.getText());
        } else if (event.getSource() == btnClear) {
            wd.hideDialog();
            ((Node) (event.getSource())).getScene().getWindow().hide();
            clearForm();
            Stage stage = new Stage();
            try {
                Parent root = FXMLLoader.load(getClass().getResource("../views/sample.fxml"));
                Scene scene2 = new Scene(root, 800, 500);
                stage.setTitle("Scene 2");
                stage.setScene(scene2);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void onShowLoading(ActionEvent event){
        wd = new WorkIndicatorDialog( ((Node)(event.getSource())).getScene().getWindow(), "Loading...");
        wd.addTaskEndNotification(result -> {
            System.out.println("RESULT"+result);
            wd=null; // don't keep the object, cleanup
        });

        wd.exec("123", inputParam -> {
            // Thinks to do...
            // NO ACCESS TO UI ELEMENTS!
            for (int i = 0; i < 20; i++) {
                try {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return new Integer(1);
        });

    }
}

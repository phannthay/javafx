package sample.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import sample.models.Linen;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    private TableView<Linen> tbLinen;
    @FXML
    private TableColumn<Linen, Integer> linenId;
    @FXML
    private TableColumn<Linen, String> category;
    @FXML
    private TableColumn<Linen, String> tagId;
    @FXML
    private TableColumn<Linen, Date> date;
    @FXML
    private TableColumn<Linen, String> department;
    @FXML
    private Button btnCount = new Button();
    @FXML
    private Button btnReset = new Button();
    @FXML
    private Button btnSubmit = new Button();
    @FXML
    private TextField tfCategory = new TextField();
    @FXML
    private TextField tfTagId = new TextField();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        linenId.setCellValueFactory(new PropertyValueFactory<>("id"));
        category.setCellValueFactory(new PropertyValueFactory<>("Category"));
        tagId.setCellValueFactory(new PropertyValueFactory<>("TagId"));
        date.setCellValueFactory(new PropertyValueFactory<>("createDate"));
        department.setCellValueFactory(new PropertyValueFactory<>("department"));

        //add your data to the table here.
        tbLinen.setItems(linenModel);
        btnCount.setOnAction(event -> {
            count();
            tbLinen.setVisible(true);
        });
        btnReset.setOnAction(event -> {
            ((Node)(event.getSource())).getScene().getWindow().hide();
            tfCategory.setText("");
            tfTagId.setText("");
        });
        btnSubmit.setOnAction(event -> addLinen());
    }

    private final ObservableList<Linen> linenModel =
            FXCollections.observableArrayList(
                    new Linen(1, "Pillow Case", "1099827772", new Date(), "Landry Plan"),
                    new Linen(2, "Pillow Case", "1099827772", new Date(), "Landry Plan"),
                    new Linen(3, "Pillow Case", "1099827772", new Date(), "Landry Plan"),
                    new Linen(4, "Pillow Case", "1099827772", new Date(), "Landry Plan"),
                    new Linen(5, "Pillow Case", "1099827772", new Date(), "Landry Plan"),
                    new Linen(6, "Pillow Case", "1099827772", new Date(), "Landry Plan")
            );

    private void addLinen() {
        if (tfCategory.getText().isEmpty()) {
            tfCategory.getStyleClass().add("setRed");
            System.out.println("Category can not empty!!");
            return;
        }
        linenModel.add(new Linen(linenModel.size() + 1, tfCategory.getText(), tfTagId.getText(), new Date(), "Linen Center"));
    }

    private void count() {
        int sum = 0;
        for (int i = 1; i < 20; i++) {
            sum += i;
        }
        System.out.println("Count:" + sum);
    }
}

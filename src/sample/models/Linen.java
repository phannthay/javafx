package sample.models;

import java.util.Date;

public class Linen {
    private int id;
    private String category;
    private String tagId;
    private Date createDate;
    private String department;

    public Linen(int id, String category, String tagId, Date createDate, String department) {
        this.id = id;
        this.category = category;
        this.tagId = tagId;
        this.createDate = createDate;
        this.department = department;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDepartemtn() {
        return department;
    }

    public void setDepartemtn(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Linen{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", tagId='" + tagId + '\'' +
                ", createDate=" + createDate +
                ", department='" + department + '\'' +
                '}';
    }
}

package sample.models.responses;

import java.util.List;

public class UserResponse {
    private List<User> results;
    private Response response;

    @Override
    public String toString() {
        return "UserResponse{" +
                "results=" + results +
                ", response=" + response +
                '}';
    }
}

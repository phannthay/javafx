package sample.models.responses;

public class User {
    private Long id;
    private String firstName;
    private String lastName;
    private String gender;
    private String phone;
    private String email;
    private String username;
    private Boolean isAdmin;
    private int type;
    private String image;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", isAdmin=" + isAdmin +
                ", type=" + type +
                ", image='" + image + '\'' +
                '}';
    }
}

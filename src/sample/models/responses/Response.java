package sample.models.responses;

public class Response {
    private int code;
    private String message;

    @Override
    public String toString() {
        return "Response{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
